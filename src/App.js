import React, { useState } from 'react'
import { ThemeProvider } from 'styled-components'
import {
  Toggle
} from './components'
import {
  lightTheme,
  darkTheme,
  GlobalStyles,
  useDarkMode
} from './tokens'

function App() {
  const [theme, handleToggle, componentMethod] = useDarkMode(window.localStorage.getItem('theme') || 'light')
  const themeMode = theme === 'light' ? lightTheme : darkTheme

  if (!componentMethod) {
    return <div />
  }

  return (
    <ThemeProvider theme={themeMode}>
      <>
        <GlobalStyles />
        <div className="app">
          <div className="app__header">
            <h1>Light/Dark Theme</h1>
            <Toggle isLight={theme} handleToggle={handleToggle} />
          </div>
          <div className="app__body">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
          </div>
        </div>
      </>
    </ThemeProvider>
  );
}

export default App;
