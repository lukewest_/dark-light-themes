import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
  *,
  *::after,
  *::before {
    box-sizing: border-box;
  }

  body {
    background: ${({ theme }) => theme.body};
    color: ${({ theme }) => theme.primaryTextColor};
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    height: 100vh;
    margin: 0;
    padding: 0;
    font-family: BlinkMacSystemFont, -apple-system, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;
    transition: all 0.25s linear;
  }

  .app {
    display: flex;
    flex-direction: column;
    gap: 1.5rem;
    
    padding: 2.5rem;

    border-radius: 20px;
    background-color: ${({ theme }) => theme.primaryColor};
    color: ${({ theme }) => theme.primaryTextColor};
    box-shadow: 0 0.16rem 0.36rem 0 rgb(0 0 0 / 13%), 0 0.03rem 0.09rem 0 rgb(0 0 0 / 11%);

    width: 100%;
    max-width: 800px;
    min-height: 100px;
    
    &__header {
      display: grid;
      grid-template-columns: auto auto;
      align-items: center;
      justify-content: space-between;
    }
  }
`