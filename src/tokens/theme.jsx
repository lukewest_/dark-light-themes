export const lightTheme = {
  body: '#ffffff',
  primaryColor: '#f8f8f8',
  secondaryColor: '#eaeaea',
  primaryTextColor: '#323232',
  borderColor: '#000000'
}

export const darkTheme = {
  body: '#000000',
  primaryColor: '#323232',
  secondaryColor: '#484848',
  primaryTextColor: '#f8f8f8',
  borderColor: '#ffffff'
}